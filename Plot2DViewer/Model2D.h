#ifndef MODEL_2D_H
#define MODEL_2D_H
#include <string>
#include <fstream>
#include "Matrix.h"
#include "AffineTransform.h"
class Model2D
{
private:
	Matrix<> Vertices;
	Matrix<int> Edges;
	Matrix<> CumulativeAT;
	Matrix<> InitialVertices;
	bool saveMode = false;
public:
	Model2D() : Vertices(), Edges() {
		CumulativeAT = Identity();
		InitialVertices = Vertices;
		if (saveMode) {
			restore();
			//Apply(CumulativeAT);
		}
	}
	Model2D(const Matrix<> Vertices, const Matrix<int> Edges) :
		Vertices(Vertices), Edges(Edges) {}
	Matrix<> GetVertices() { return Vertices; }
	Matrix<int> GetEdges() { return Edges; }

	double getVerticeX(int index) {
		return Vertices.operator()(1, index);
	}

	int getEdgesCount() {
		return Edges.getRowsCount();
	}

	double getVerticeY(int index) {
		return Vertices.operator()(2, index);
	}

	void Apply(Matrix<> transformation) {
		this->CumulativeAT = transformation * this->CumulativeAT;
		this->Vertices = transformation * this->Vertices;
		if (saveMode) {
			save();
		}
	}

	void SetTestSet() {
		//������
		//double v[21] = { 3,4,5,6,7,5,5,4,4,4,4,4,1,7,1,1,1,1,1,1,1};
		//Matrix<> V(3, 7, v);
		//int e[20] = { 1,7,2,7,3,7,4,7,5,7,1,6,2,6,3,6,4,6,5,6 };
		//Matrix<int> E(10, 2, e);

	}

	void save() {
		ofstream fout("Saved.txt", std::ios::trunc);
		fout << "3 " << CumulativeAT.getRowsCount() << " ";
		for (int i = 0; i < Vertices.getRowsCount() * 3; i++) {
			fout << CumulativeAT.operator()(1, i+1) << " ";
		}
	}

	void restore() {
		ifstream fin("Saved.txt");
		if (fin.peek() != std::ifstream::traits_type::eof()) {
			int rows, cols;
			fin >> rows >> cols;
			double * vers = new double[rows*cols];
			for (int i = 0; i < rows*cols; i++) {
				fin >> vers[i];
			}
			Matrix<double> modifications(rows, cols, vers);
			this->CumulativeAT = modifications;
			
			Apply(CumulativeAT);
		}
	}

	Model2D(string VerticesPath, string EdgesPath) {
		ifstream fin(VerticesPath);
		int rows, cols;
		fin >> rows >> cols;
		double * vers = new double[rows*cols];
		for (int i = 0; i < rows*cols; i++) {
		fin >> vers[i];
		}
		Matrix<double> vertices(rows, cols, vers);
		this->Vertices = vertices;

		ifstream findl(EdgesPath);
		findl >> rows;
		int * edgy = new int[2 * rows];
		for (int i = 0; i < 2 * rows; i++) {
		findl >> edgy[i];
		}
		Matrix<int> edges(rows, 2, edgy);
		this->Edges = edges;

		CumulativeAT = Identity();
		InitialVertices = Vertices;

		if (saveMode) {
			restore();
		}
	}
};
#endif MODEL_2D_H