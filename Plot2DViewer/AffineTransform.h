#ifndef AFFINE_TRANSFORM_H
#define AFFINE_TRANSFORM_H

Matrix<> Translation(double x, double y)
{
	double T[9] = {
		1, 0, x,
		0, 1, y,
		0, 0, 1 };
	return Matrix<>(3, 3, T);
}

// ����� ��������� ����������� �������, ������������ ������� ������� ��:
// Identity() - ������������� ��;
// Rotation(t) - ������� �� ���� t;
// Rotation(c, s) - ������� �� ����, ������� � ����� �������� ��������������� ��������� c � s;
// Scaling(kx, ky) - ���������������;
// Mapping (��������� ���� ���������) - �� �������, ��� �������� ������ ��������� ����� ������������ ������� Scaling.

// � ���������� ������� ������������ ����������� ������� �� ������.
// �������������� ���� � ������ �������� �������� ������.
// ��������, ����� �������, ��������� ��������� �������,
// � ����������� ���������������� ��������� � �������������.

Matrix<> Identity() {
	double T[9] = {
		1, 0, 0,
		0, 1, 0,
		0, 0, 1 };
	return Matrix<>(3, 3, T);
}

Matrix<> Rotation(double t) {
	double T[9] = {
		cos(t), -sin(t), 0,
		sin(t), cos(t), 0,
		0, 0, 1 };
	return Matrix<>(3, 3, T);
}

Matrix<> Rotation(double c, double s) {
	double sinus = s / sqrt(c * c + s * s);
	double cosinus = c / sqrt(c * c + s * s);
	double T[9] = {
		cosinus, -sinus, 0,
		sinus, cosinus, 0,
		0, 0, 1 };
	return Matrix<>(3, 3, T);
}

Matrix<> Scaling(double kx, double ky) {
	double T[9] = {
		kx, 0, 0,
		0, ky, 0,
		0, 0, 1 };
	return Matrix<>(3, 3, T);
}

Matrix<> MirrorX() {
	double T[9] = {
		1, 0, 0,
		0, -1, 0,
		0, 0, 1 };
	return Matrix<>(3, 3, T);
}

Matrix<> MirrorY() {
	double T[9] = {
		-1, 0, 0,
		0, 1, 0,
		0, 0, 1 };
	return Matrix<>(3, 3, T);
}

Matrix<> Mirror() {
	double T[9] = {
		-1, 0, 0,
		0, -1, 0,
		0, 0, 1 };
	return Matrix<>(3, 3, T);
}

Matrix<> RotateOnPoint(double t, double x, double y) {
	return Translation(x, y) * Rotation(t) * Translation(-x, -y);
}

Matrix<> MirrorXOnPoint(double x, double y) {
	return Translation(x, y) * MirrorX() * Translation(-x, -y);
}
Matrix<> MirrorYOnPoint(double x, double y) {
	return Translation(x, y) * MirrorY() * Translation(-x, -y);
}
Matrix<> MirrorOnPoint(double x1, double y1, double x2, double y2) {
	double c, d, l,m;
	double x = (x1 + x2) / 2;
	double y = (y1 + y2) / 2;

	l = y2 - y1;
	m = x2 - x1;

	c = m / sqrt(l*l + m * m);
	d = l / sqrt(l*l + m * m);

	return Translation(x1, y1) * Rotation(acos(d)) * MirrorY()  * Rotation(-acos(d))  * Translation(-x1, -y1);
}

#endif AFFINE_TRANSFORM_H
