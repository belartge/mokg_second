#ifndef MODEL_2D_H
#define MODEL_2D_H
#include <string>
#include <fstream>
#include "Matrix.h"
#include "AffineTransform.h"
class Model2D
{
private:
	Matrix<> Vertices;
	Matrix<int> Edges;
	Matrix<> CumulativeAT;
	Matrix<> InitialVertices;
public:
	Model2D() : Vertices(), Edges() {
		CumulativeAT = Identity();
		InitialVertices = Vertices;
	}
	Model2D(const Matrix<> Vertices, const Matrix<int> Edges) :
		Vertices(Vertices), Edges(Edges) {}
	Matrix<> GetVertices() { return Vertices; }
	Matrix<int> GetEdges() { return Edges; }

	double getVerticeX(int index) {
		return Vertices.operator()(1, index);
	}

	double getVerticeY(int index) {
		return Vertices.operator()(2, index);
	}
	
	void Apply(Matrix<> transformation) {
		this->CumulativeAT = transformation * this->CumulativeAT;
		this->Vertices = transformation * this->Vertices;
	}

	Model2D(string VerticesPath, string EdgesPath) {
		/*ifstream fin(VerticesPath);
		int rows, cols;
		fin >> rows >> cols;
		double * vers = new double[rows*cols];
		for (int i = 0; i < rows*cols; i++) {
			fin >> vers[i];
		}
		Matrix<double> vertices(rows, cols, vers);
		this->Vertices = vertices;

		ifstream findl(EdgesPath);
		findl >> rows;
		int * edgy = new int[2 * rows];
		for (int i = 0; i < 2 * rows; i++) {
			findl >> edgy[i];
		}
		Matrix<int> edges(rows, 2, edgy);
		this->Edges = edges;

		CumulativeAT = Identity();
		InitialVertices = Vertices;*/
	}
};
#endif MODEL_2D_H